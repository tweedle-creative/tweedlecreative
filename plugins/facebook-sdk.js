//----------------------------------------------------------------------------------------------------------------------
// facebook-sdk.js
//----------------------------------------------------------------------------------------------------------------------

import Vue from 'vue';

//----------------------------------------------------------------------------------------------------------------------

const vueFB = {
    install(Vue, options)
    {
        Vue.FB = undefined;

        // Facebook SDK init function
        (function(d, s, id)
        {
            let js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)){ return }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        Vue.FB = {
            SDK: undefined,
            _init()
            {
                Vue.FB.SDK.init(options);
                Vue.FB.SDK.AppEvents.logPageView();
                window.removeEventListener('fb-sdk-ready', this);
            },
            init()
            {
                if(!Vue.FB.SDK)
                {
                    window.addEventListener('fb-sdk-ready', Vue.FB._init);
                }
                else
                {
                    Vue.FB._init();
                } // end if
            }
        };

        window.fbAsyncInit = () =>
        {
            Vue.FB.SDK = FB;
            window.dispatchEvent(new Event('fb-sdk-ready'))
        } // end fbAsyncInit
    } // end install
};

Vue.use(vueFB, {
    appId: '425825227777188',
    autoLogAppEvents: true,
    xfbml: true,
    version: 'v2.9'
});

//----------------------------------------------------------------------------------------------------------------------
