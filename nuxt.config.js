import pkg from './package'

export default {
  mode: 'spa',

  /*
  ** Headers of the page
  */
  head: {
    title: 'Tweedle Creative',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      { hid: 'description', name: 'description', content: 'Tweedle Creative will bring humor, joy, and laughter into a difficult world. We will demonstrate integrity, facing the truth without fear. We will give generously and be a blessing to others out of what we have received. We will foster communities that demonstrate these values because we believe that each individual is worthy of respect and has an innate need to be known and to know others.' },
      { hid: 'keywords', name: 'keywords', content: 'podcast, audio editing, social media, web development, empowerment, consulting, marketing' },
    ],
    link: [
        { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Chivo%7CForum%7CLimelight'  },
        { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', integrity: "sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN", crossorigin:"anonymous" }
    ]
  },

  /*
  ** Global CSS
   */
  css: [ ],

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
      { src: '~/plugins/facebook-sdk.js', mode: 'client' }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt'
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
